module.exports = {
  pgdbHost: process.env.FMS_PGDB_HOST || process.env.PGDB_HOST || "localhost",
  pgdbPort: process.env.FMS_PGDB_PORT || process.env.PGDB_PORT || "65535",
  pgdbIsAuth:
    process.env.FMS_PGDB_IS_AUTH || process.env.PGDB_IS_AUTH || "true",
  pgdbUsername:
    process.env.FMS_PGDB_USERNAME || process.env.PGDB_USERNAME || "master",
  pgdbPassword:
    process.env.FMS_PGDB_PASSWORD ||
    process.env.PGDB_PASSWORD ||
    "340129h9cqw60432bnf",

  pgDbName: process.env.PGDB_NAME || "fms-app",

  appPort: process.env.FMS_API_PORT || 3032,
  appHost: process.env.FMS_API_HOST || "0.0.0.0",

  appEnv: process.env.NODE_ENV || process.env.FMS_API_ENV || "dev",
  appLog: process.env.FMS_API_LOG || "dev",

  accessTokenSecret:
    process.env.FMS_API_ACCESS_TOKEN_SECRET ||
    "5A548FD77040AEA80DB67386CF48C0F228E56620A367712E72444DA6379BF65A",
  refreshTokenSecret:
    process.env.FMS_API_REFRESH_TOKEN_SECRET ||
    "6978EB463E14E65513F0E2FF422D2E597EC5AF48D734A59CC2472933550521DC",

  loginAPIServer:
    process.env.FMS_DS_API ||
    process.env.DS_API ||
    "http://10.218.218.2:17016/api/v1",
};
