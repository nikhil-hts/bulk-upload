"use strict";
//Import Schema
const Schema = require("./Schema");
module.exports = (sequelize, DataTypes) => {
  const deviceSchema = Schema(DataTypes).UploadSchema;
  const Device = sequelize.define("test", deviceSchema);

  Device.associate = function (models) {
    // associations can be defined here
  };
  return Device;
};
