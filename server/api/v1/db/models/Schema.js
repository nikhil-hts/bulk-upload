//Import Sequelize
const Sequelize = require("sequelize");

module.exports = (input = {}) => {
  const Schema = {
    Upload: {
      id: {
        type: input.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      Series_reference: input.STRING,
      Period: input.INTEGER,
      Data_value: input.INTEGER,
      Suppressed: input.STRING,
      STATUS: input.STRING,
      UNITS: input.STRING,
      Magnitude: input.INTEGER,
      Subject: input.STRING,
      Group: input.STRING,
      Series_title_1: input.STRING,
      Series_title_2: input.STRING,
      Series_title_3: input.STRING,
      Series_title_4: input.STRING,
      Series_title_5: input.STRING,
    },
  };

  //Upload Schema final
  const { ...UploadSchema } = Schema.Upload;

  //Return
  return {
    Schema,
    UploadSchema,
  };
};
