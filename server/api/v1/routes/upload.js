"use strict";
//Import Koa Router
const Router = require("koa-router");
//Instantiate Router
const router = new Router();
//Import Controller
const Controller = require("./../controllers/upload");
/*
 
! Upload Routes

*/

router.use("/file", Controller.getAllData);
//Export
module.exports = router;
