"use strict";
//Import Koa Router
const Router = require("koa-router");
//Import Server Routes
const Upload = require("./upload");
//Instantiate Router
const router = new Router({
  prefix: "/api/v1",
});
//Test Route
router.get("/status", (ctx, next) => {
  ctx.body = "Running!";
});
//User Routes
router.use("/upload", Upload.routes());
//Export
module.exports = router;
