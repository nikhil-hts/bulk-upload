"use strict";

// //Import Sequelize
const { Sequelize, Op, QueryTypes } = require("sequelize");
// //Import PG Models
const DB = require("./../db/models/pg");
// //Import Axios
// const Axios = require("axios");
// //Upload Schema
const Schema = require("./../db/models/Schema");
//Import xlsx
const XLSX = require("xlsx");

//Load XLSX file
const workbook = XLSX.readFile(
  "O:/bulkupload/server/api/v1/controllers/workbook.xlsx"
);
const sheets = workbook.SheetNames;
const data = XLSX.utils.sheet_to_json(workbook.Sheets[sheets[0]]);

//Controller
(async () => {
  try {
    const res = await DB.test.bulkCreate(data);
    if (res) {
      console.log(`${res.length} records saved`);
    }
  } catch (error) {
    console.log(error);
  }
})();
//Controller
module.exports = class DataHandler {
  //Get All Data
  static getAllData = async (ctx, next) => {
    try {
    } catch (error) {
      console.log(err);
    }
  };
};
